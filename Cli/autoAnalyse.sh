#!/bin/bash

echo "Téléchargement des fichiers...";
curl http://www.whatpackage.tech/public/ressources/parser.sh -o parser.sh;
curl http://www.whatpackage.tech/public/ressources/whatPack.sh -o whatPack.sh;
chmod +x parser.sh whatPack.sh;

echo "Lancement de l'analyse...";
./parser.sh;
