#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Usage: ./whatPack.sh <command>";
    exit 1;
fi

# Variables
# Colors
RED="\033[1;31m";
NOR="\033[0m";

# Core
lang='';
cmd=$1;
fileBuff='/tmp/whatPak.txt';


# infos
name='';
repo='';
desc='';
url='';
lic='';
downS='';
instS='';
maint='';

# recuperation de la langue du systeme
lang=$(locale | grep "LANG" | cut -d '=' -f 2 | cut -d '.' -f 1);

function getInfos() {
    pacman -Si $1 > $fileBuff;
    if [ "$lang" == "en_US" ] || [ "$lang" == "en_UK" ]; then
        name=$(grep 'Name' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        repo=$(grep 'Repository' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        desc=$(grep 'Description' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        url=$(grep 'URL' $fileBuff | tr -d ' ' | cut -c5-);
        lic=$(grep 'Licenses' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        downS=$(grep 'Download Size' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        instS=$(grep 'Installed Size' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        maint=$(grep 'Packager' $fileBuff | cut -d ":" -f 2 | cut -c2-);
    elif [ "$lang" == "fr_FR" ]; then
        name=$(grep 'Nom' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        repo=$(grep 'Dépôt' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        desc=$(grep 'Description' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        url=$(grep 'URL' $fileBuff | tr -d ' ' | cut -c5-);
        lic=$(grep 'Licences' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        downS=$(grep 'Taille du téléchargement' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        instS=$(grep 'Taille installée' $fileBuff | cut -d ":" -f 2 | cut -c2-);
        maint=$(grep 'Validé par' $fileBuff | cut -d ":" -f 2 | cut -c2-);
    else
        echo -e $RED"Your system language is not supported yet..."$NOR;
        exit 1;
    fi

    json="{
        \"Name\": \"$name\",
        \"Repository\": \"$repo\",
        \"Description\": \"$desc\",
        \"URL\": \"$url\",
        \"Licenses\": \"$lic\",
        \"Download Size\": \"$downS\",
        \"Installed Size\": \"$instS\",
        \"Maintainer\": \"$maint\",
        \"Commands\": [\"$cmd\"]
    }";

    echo -e "$json";
}

pkgs=$(pacman -Foq /usr/bin/$cmd 2> /dev/null);
number=$(echo $pkgs | wc -w);
case $number in
    0)
        ;;
    1)
        pkg=$pkgs;
        getInfos $pkg;
        echo '';
        ;;
    *)
        i=0;
        for pkg in `echo -n $pkgs`
        do
            i=$(($i + 1));
            getInfos $pkg;
        done
esac
