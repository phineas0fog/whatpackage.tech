#!/bin/bash

# variables
# Colors
RED="\033[1;31m";
GREEN="\033[1;32m";
BLUE="\033[1;36m";
YEL="\033[1;33m";
NOR="\033[0m";

# files
remoteCommandListFile="remoteCommandList.txt";
localCommandListFile="localCommandList.txt";
localCommandListToParse="localCommandListToParse.txt";
jsonFile="output.json";
succeedListFile='succeed.log';
failedListFile='failed.log';
archive='out.tar';

# core
count='';
i=1;
remoteCommandList='';
url='http://www.whatpackage.tech';
out='';
succeed=0;
failed=0;

function clean() {
    rm $archive $remoteCommandListFile $localCommandListFile $localCommandListToParse $jsonFile $succeedListFile $failedListFile;
}

clean 2>/dev/null;

echo -e $BLUE"Récupération de la liste des commandes déjà indéxées..."$NOR;
remoteCommandList=$(curl -H 'Content-Type: application/json' "$url/commandList");
echo $remoteCommandList | tr -d ',' | tr -d '[]' | tr -s "\"\"" "\n"  | sort | uniq > $remoteCommandListFile;

echo -e $BLUE"Création de la liste des commandes locales ..."$NOR;
for command in /usr/bin/*; do
    echo $command | cut -d '/' -f 4 >> $localCommandListFile;
done

echo -e $BLUE"Création de la liste des nouvelles commandes..."$NOR;
comm -13 $remoteCommandListFile $localCommandListFile | uniq > $localCommandListToParse;

count=$(wc -w $localCommandListToParse | cut -d ' ' -f 1);
echo -e $BLUE"$count commandes vont être analysées..."$NOR;


echo -e $BLUE"Début de l'analyse !"$NOR;
for command in $(cat $localCommandListToParse); do
    echo -e "----- Command : $BLUE$command$NOR \t\t\t $GREEN$i$NOR / $RED$count$NOR - rem : $YEL$(($count - $i))$NOR";
    out=$(./whatPack.sh $command);

    if [ $? -ne 0 ]; then
        echo -e $RED"Un problème est survenu lors de l'analyse."$NOR;
        exit 1;
    fi

    if [ "$out" == "" ]; then
        echo -e $YEL"Échoué"$NOR;
        failed=$(($failed + 1));
        echo $command >> $failedListFile;
    else
        echo -e $GREEN"Réussi"$NOR;
        succeed=$(($succeed + 1));
        echo $command >> $succeedListFile;
        echo $out;
        echo $out >> $jsonFile;
    fi

    i=$(($i + 1));
done

echo -e "--------------------";
echo -e $BLUE"Processus terminé, rapport :"$NOR;
echo -e "\t Réussites : "$GREEN$succeed$NOR " / " $count;
echo -e "\t Échecs : "$RED$failed$NOR " / " $count;
echo -e "--------------------";
echo -e $BLUE"Empaquetage des résultats..."$NOR;

tar -czvf $archive $remoteCommandListFile $localCommandListFile $localCommandListToParse $jsonFile $succeedListFile $failedListFile;

if [ ! -f $archive ]; then
    echo -e $RED"Un problème est survenu lors de l'empaquetage des fichiers..."$NOR;
    echo "Veuillez créer l'archive contenant l'ensemble des fichiers .txt et .json avec la"
    echo "commande 'tar -czvf out.tar *.log *.txt *.json'. Vous pourrez les supprimer ensuite."
    exit 1;
else
    curl "$url/public/ressources/badge.png" -o "merci.png";
    clean;
fi

echo -e "--------------------";
echo -e $BLUE"Terminé ! Merci de votre contribution"$NOR;
