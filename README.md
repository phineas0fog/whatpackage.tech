![banner](Graphics/banner.png)

# [www.whatpackage.tech](www.whatpackage.tech)

Trouvez le paquet qui contient la commande que vous cherchez !

# Documentation
## Structure de données *(json)*
```
    ﻿[
      {
        "Name": "<nom du paquet>",
        "Repository": "<nom du repo>",
        "Description": "<description>",
        "URL": "<lien du paquet>",
        "Licenses": "<license>",
        "Download Size": "<taille du téléchargement>",
        "Installed Size": "<taille du paquet installé>",
        "Maintainer": "<nom du mainteneur> <mail du mainteneur>",
        "Commands": [
          "<commande>"
        ]
      }
    ]
```

## Récupération des informations
### Paquet correspondant
`pacman -Foq /usr/bin/<commande>`

### Informations sur le paquet
`pacman -Si <nom du paquet>`
