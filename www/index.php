<?php

    define('ROOT', __DIR__);

    require ROOT . '/Autoloader.php';

    www\Autoloader::register();

    use www\core\Router;

    $router = Router::getInstance();
    $router->init();
