<?php
    $COUNT_FILE = "count_data.txt";
    if (file_exists($COUNT_FILE)) {
        $fp = fopen("$COUNT_FILE", "r+");
        flock($fp, 1);
        $visits = fgets($fp, 4096);
        $visits += 1;
        fseek($fp,0);
        fputs($fp, $visits);
        flock($fp, 3);
        fclose($fp);
    } else {
        $visits = 0;
    }
