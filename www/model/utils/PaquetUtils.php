<?php

namespace www\model\utils;

use www\gateway\Gateway;

class PaquetUtils {
    private $gw;

    public function __construct() {
        $this->gw = new Gateway();
    }

    public function searchByCommand(String $command) {
        return $this->gw->searchByCommand($command);
    }

    public function searchByPackage(String $package) {
        return $this->gw->searchByPackage($package);
    }

    public function countAll() {
        return $this->gw->countAll();
    }

    public function getCommandList() {
        return $this->gw->getCommandList();
    }
}
