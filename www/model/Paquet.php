<?php

namespace www\model;

class Paquet {
        public $name;
        public $repository;
        public $description;
        public $url;
        public $licenses;
        public $downloadSize;
        public $installedSize;
        public $maintainer;
        public $mail;
        public $commands;

        public function __construct(String $name, String $repository, String $description, String $url, String $licenses, String $downloadSize, String $installedSize, String $maintainer, String $mail, String $commands) {
            $this->name          = $name;
            $this->repository    = $repository;
            $this->description   = $description;
            $this->url           = $url;
            $this->licenses      = $licenses;
            $this->downloadSize  = $downloadSize;
            $this->installedSize = $installedSize;
            $this->maintainer    = $maintainer;
            $this->mail          = $mail;
            $this->commands      = $commands;
        }
    }
