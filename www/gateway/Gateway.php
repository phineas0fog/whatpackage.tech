<?php

namespace www\gateway;

use Elasticsearch\ClientBuilder;
use www\core\Config;
use www\model\Paquet;
use www\core\Validation;

require 'vendor/autoload.php';

class Gateway {
    private $client;

    public function __construct() {

        $hosts[] = Config::getInstance()->get('elastic')['url'];

        try {
            $clientBuilder = ClientBuilder::create();
            $clientBuilder->setHosts($hosts);
            $this->client = $clientBuilder->build();
        } catch (\Exception $e) {
            throw new \Exception(42);
        }
    }

    public function searchByCommand($command) {
        $json = "{
                    \"from\" : 0, \"size\" : 1000,
                      \"query\": {
                            \"fuzzy\": { \"Commands\" : \"$command\" }
                      }
                }";

        $params = [
            'index' => Config::getInstance()->get('elastic')['index'],
            'type' => Config::getInstance()->get('elastic')['type'],
            'body' => $json
        ];

        $response = $this->client->search($params);
        $results = [];
        foreach($response['hits']['hits'] as $result) {
            $p = new Paquet(
                $result['_source']['Name'],
                $result['_source']['Repository'],
                $result['_source']['Description'],
                $result['_source']['URL'],
                $result['_source']['Licenses'],
                $result['_source']['Download Size'],
                $result['_source']['Installed Size'],
                // Validation::validate($result['_source']['Maintainer'], 'TEXT'),

                explode('<', $result['_source']['Maintainer'])[0],

                str_replace('>', '', explode('<', $result['_source']['Maintainer'])[1]),

                $result['_source']['Commands'][0]
            );
            $results[] = $p;
        }
        return $results;
    }

    public function searchByPackage($package) {
        $json = "{
                    \"from\" : 0, \"size\" : 1000,
                     \"query\": {
                            \"fuzzy\": {
                                \"Name\" : {
                                    \"value\": \"$package\",
                                    \"fuzziness\" : 1
                                }
                            }
                      }
                }";

        $params = [
            'index' => Config::getInstance()->get('elastic')['index'],
            'type' => Config::getInstance()->get('elastic')['type'],
            'body' => $json
        ];

        $response = $this->client->search($params);
        $results = [];
        foreach($response['hits']['hits'] as $result) {
            $p = new Paquet(
                $result['_source']['Name'],
                $result['_source']['Repository'],
                $result['_source']['Description'],
                $result['_source']['URL'],
                $result['_source']['Licenses'],
                $result['_source']['Download Size'],
                $result['_source']['Installed Size'],
                // Validation::validate($result['_source']['Maintainer'], 'TEXT'),

                explode('<', $result['_source']['Maintainer'])[0],

                str_replace('>', '', explode('<', $result['_source']['Maintainer'])[1]),

                $result['_source']['Commands'][0]
            );
            $results[] = $p;
        }
        return $results;
    }

    public function countAll() {
        return $this->client->count([])['count'];
    }

    public function getCommandList() {
        $json = '{
                	"from" : 0, "size" : 10000,
                	"_source": ["Commands"],
                    "query" : {
                        "match_all" : {}
                    }
                }';

        $params = [
            'index' => Config::getInstance()->get('elastic')['index'],
            'type' => Config::getInstance()->get('elastic')['type'],
            'body' => $json
        ];
        $response = $this->client->search($params);
        $results = [];
        foreach($response['hits']['hits'] as $result) {
            if($result['_source']['Commands'][0] == NULL) {
                continue;
            }
            else
                $results[] = $result['_source']['Commands'][0];

        }
        return $results;
    }
}
