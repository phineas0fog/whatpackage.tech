<?php
    /**
     * General config file
     *
     * ['view']['name'] -> location for each view
     */
    $conf['rep'] = __DIR__.'/../';

    $conf['basePage']            = 'template/BASE.php';

    $conf['view']['homeView']    = 'view/home.php';
    $conf['view']['results']     = 'view/results.php';
    $conf['view']['error']       = 'view/error.php';
    $conf['view']['about']       = 'view/about.php';
    $conf['view']['contrib']     = 'view/contrib.php';
    $conf['view']['thanks']      = 'view/thanks.php';

    return $conf;
