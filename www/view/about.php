<?php include 'component/header.php' ?>

<div class='mainContainer grid cols3Center'>
    <?php include 'component/sidebar.php' ?>


	<div class="col2 card about">
		<h1>À propos du site</h1>
		<p class="">
			Parfois, on a besoin d'utiliser une commande car on l'a vue sur internet,ou que quelqu'un nous en a parlé, mais on ne trouve pas le paquet qu'ilfaut installer pour avoir accès à cette commande... Voila donc le but de ce site !Il suffit de taper le nom d'une commande pour avoir le nom du paquet la fournissant,ainsi que diverses informations à son propos.Ce site est maintenu par son propriétaire et sera le plus possible mis àjour avec de nouvelles commandes.
		</p>
		<h1>Mentions légales</h1>
		<h2>Auteur</h2>
		<p>
			Le présent site a été entièrement conçu et réalisé par Evrard Van Espen
			<mail>(<a href="mailto:e.vanespen@protonmail.com">e.vanespen@protonmail.com</a>)</mail> qui est le seul propriétaire.
		</p>
		<h2>Graphiques</h2>
		<p>
			Le design et la charte graphique du présent site sont placé.e.s sous licence
			<i>Creative Commons BY-NC-SA</i>
			. Vous pouvez donc :
			<ul>
				<li>- <b>partager :</b> copier, distribuer et communiquer le matériel par tous moyens et sous tous formats</li>
				<li>- <b>Adapter :</b> remixer, transformer et créer à partir du matériel </li>
					</ul>
		</p>
		<p>
			Aux conditions suivantes :
			<ul>
				<li>- <b>Attribution :</b> vous devez créditer l'oeuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées
	                 à l'oeuvre. Vous devez indiquer ces informations partous les moyens raisonnables, sans toutefois suggérer
	                 que l'offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.</li>
				<li>- <b>Pas d’Utilisation Commerciale :</b> Vous n'êtes pas autorisé à faire un usage commercial
	                de cette Oeuvre, tout ou partie du matériel la composant.</li>
				<li>- <b>Partage dans les mêmes Conditions :</b> dans le cas où vous
	                 effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'oeuvre originale,
	                 vous devez diffuser l'oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'oeuvre originale a été diffusée.</li>
			</ul>
		</p>
		<p>
			<a href="https://creativecommons.org/licenses/by-nc-sa/2.0/fr/">https://creativecommons.org/licenses/by-nc-sa/2.0/fr/</a>
		</p>
		<h2>Icônes</h2>
		<p>Les icônes proviennent du site <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> et sont sous license <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a> :
			<ul>
				<li>- loupe du logo du site : <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a></li>
				<li>- <i>package</i> du logo du site : <a href="https://www.flaticon.com/authors/simpleicon" title="SimpleIcon">SimpleIcon</a></li>
				<li>- loupe du bouton de recherche : <a href="http://www.freepik.com" title="Freepik">Freepik</a></li>
			</ul>
		</p>

		<h2>Code</h2>
		<p>
			Le code du présent site est placé sous licence
			<i>GNU AGPLv3</i>
			avec une close supplémentaire :
			<ul>
				<li>- <b>tout usage commercial du présent site est interdit sauf autorisation écrite du propriétaire</b>.</li>
			</ul>
		</p>
		<p>
			<a href="https://www.gnu.org/licenses/agpl-3.0.txt">https://www.gnu.org/licenses/agpl-3.0.txt</a>
		</p>
	</div>
</div>
