<?php include 'component/header.php' ?>

<div class='mainContainer grid cols3Center'>
    <?php include 'component/sidebar.php' ?>

	<div class="col2 card about">
		<h1>Contribution</h1>
        <p>
            Bienvenue sur le guide de contribution. Avant de commencer, laissez moi vous remercier pour votre future contribution.
            Ce guide va vous expliquer pas à pas comment contribuer.
        </p>

        <div class="warning">
            Pour le moment, seules les personnes utilisant un système basé sur <i>ArchLinux (Manjaro, Antergos, etc)</i>
            peuvent contribuer car les scripts utilisent le gestionnaire de paquets
             <i>pacman</i> pour analyser les commandes.
        </div>

        <div class="info">
            Suivant le nombre de commandes à analyser, la procécure peu durer plus ou moins longtemps. <br>
            L'analyse d'une commande prend en moyenne 1 seconde et le nombre moyen de commandes à analyser se situe
            aux alentours de 250.
        </div>

        <h2>Guide</h2>
        <p>Toutes les sources peuvent être récupérées sur le dépôt <i>git</i> du projet :
            <a href="<?= www\core\Config::getInstance()->get('repo') ?>" target="_blank" rel="noopener noreferrer" ><?= www\core\Config::getInstance()->get('repo') ?></a>
        </p>

        <h3>En une ligne</h3>
        <p>
            Cette commande va télécharger les deux scripts nécessaires et lancer l'analyse automatiquement.
        </p>
        <button class="btn btnDark btnFold" onclick="toggle_visibility('autoAnalyse');">Afficher / masquer le code</button>
        <pre class="codeBlock hidden" id="autoAnalyse"><code class="bash">
                <?php echo file_get_contents('public/ressources/autoAnalyse.sh') ?>
        </code></pre>
        <code>curl <?= $root ?>/public/ressources/autoAnalyse.sh | bash</code>
        <br><br>

        <h3>Scripts d'analyse</h3>
        <p>
            Les scripts nécessaires peuvent être trouvés dans le dossier <code>'Cli'</code> à la racine du dépôt <i>git</i> et sont
            nommés <code>parser.sh</code> et <code>whatPak.sh</code>.
        </p>

        <h4><code>whatPack.sh</code></h4>
        <p>
            Ce script va, pour une commande passée en argument, chercher le ou les paquets qui la fournis(sent) à l'aide
            de la commande <code>pacman -Foq &lt;commande></code> puis va ensuite récupérer les informations à propos de ce paquet
            avec la commande <code>pacman -Si &lt;nom du paquet></code>. Les informations récupérées sont le nom du paquet, le dépôt <i>ArchLinux</i>
            dans lequel il se trouve, sa description, sa licence, sa taille lors du téléchargement ainsi que sa taille une fois installé ainsi que le nom
            de la personne qui a mis en ligne le paquet (souvent le déceloppeur / mainteneur principal).
        </p>
        <button class="btn btnDark" onclick="window.location.href='<?= $root ?>/public/ressources/whatPack.sh'">Télécharger le fichier</button>
        <button class="btn btnDark btnFold" onclick="toggle_visibility('whatpack');">Afficher / masquer le code</button>
            <pre class="codeBlock hidden" id="whatpack"><code class="bash">
                    <?php echo file_get_contents('public/ressources/whatPack.sh') ?>
            </code></pre>
        <br><br>


        <h4><code>parser.sh</code></h4>
        <p>
            Ce script va récupérer la liste des commandes déjà indéxées sur le site, ainsi que la liste des commandes disponnibles sur votre machine dans <code>/usr/bin</code>.
            Une fois ces listes créées, le script va les comparer de façon à déterminer la liste des nouvelles commandes (celles que vous possédez et qui ne sont pas sur le site)
            puis va appeller le script précédent pour chacune d'elles.
        </p>
        <button class="btn btnDark" onclick="window.location.href='<?= $root ?>/public/ressources/parser.sh'">Télécharger le fichier</button>
        <button class="btn btnDark btnFold" onclick="toggle_visibility('parser');">Afficher / masquer le code</button>
            <pre class="codeBlock hidden" id="parser" ><code class="bash">
                    <?php echo file_get_contents('public/ressources/parser.sh') ?>
            </code></pre>
        <br><br>

        <h3>Fichiers de sortie</h3>
        Au cours du processus d'analyse, plusieurs fichiers seront créés :
        <ul>
            <li><code>remoteCommandList.txt</code> : liste des commandes déjà indéxées</li>
            <li><code>localCommandList.txt</code> : liste des commandes de votre système</li>
            <li><code>localCommandListToParse.txt</code> : liste des commandes de votre système à analyser</li>
            <li><code>output.json</code> : résultats sous forme d'objets <i>json</i></li>
            <li><code>failed.log</code> : liste des commandes dont le paquet correspondant n'a pas été trouvé</li>
            <li><code>succeed.log</code> : liste des commandes dont le paquet correspondant a été trouvé</li>
        </ul>
        <br><br>

        <h3>Phase finale</h3>
        <p>
            Une fois toutes les commandes analyées et les résultats obtenus, une archive nommée "<code>out.tar</code>" est crée
            et c'est cette archive que vous transmettrez. Celle ci contient simplement les fichiers énoncés précédemment.
        </p>

        <h4>Envoi des résultats</h4>
        <form action="<?= $root ?>/uploadResults" method="post" enctype="multipart/form-data">
            <input type="file" name="fileToUpload" id="fileToUpload" class="btn btnDark" required>
            <input type="submit" value="Envoyer" name="submit" class="btn btnDark">
        </form>

	</div>
</div>

<link rel="stylesheet" href="public/min/gruvbox-dark.min.css">
<script src="public/min/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
