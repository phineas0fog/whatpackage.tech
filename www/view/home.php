<?php include 'component/header.php' ?>

<div class='mainContainer grid cols3Center'>
    <?php include 'component/sidebar.php' ?>
    <div class="col2 card cardHome">
        <h1>Chercher par <b>commande</b></h1>
        <p>La recherche par commande vous permet de trouver le paquet qui fourni la commande que vous cherchez.</p>
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

    <div class="col2 card cardHome">
        <h1>Chercher par <b>paquet</b></h1>
        <p>La recherche par paquet vous permet de trouver les commandes fournies par le paquet que vous indiquez.</p>
        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>
    </div>
</div>
