<?php include 'component/header.php' ?>

<div class='mainContainer grid cols3Center'>
    <?php include 'component/sidebar.php' ?>
    <?php if(isset($params['results']) AND sizeof($params['results']) > 0) : ?>
            <?php foreach($params['results'] as $result) : ?>
                <div class="col2 card">
                    <h1 class="title"><span class="packName"><?= $result->name?></span> / <span class="command"><?= $result->commands ?></span></h1>
                    <ul class="cardPack">
                        <li><h1 class="detLabl">Dépot : <i>(Archlinux)</i></h1>                     <p class="detContent"><?= $result->repository; ?></p></li>
                        <li><h1 class="detLabl">Description : </h1>               <p class="detContent"><?= $result->description; ?></p></li>
                        <li><h1 class="detLabl">Lien : </h1>                      <p class="detContent"> <a target="_blank" href="<?= $result->url; ?>"><?= $result->url; ?></a></p></li>
                        <li><h1 class="detLabl">License(s) : </h1>                <p class="detContent"><?= $result->licenses; ?></p></li>
                        <li><h1 class="detLabl">Taille du téléchargement : </h1>  <p class="detContent"><?= $result->downloadSize; ?></p></li>
                        <li><h1 class="detLabl">Taille de l'installation : </h1>  <p class="detContent"><?= $result->installedSize; ?></p></li>
                        <li><h1 class="detLabl">Packager : </h1>                  <p class="detContent"><?= $result->maintainer; ?> <i class="mail">(
                            <?php if(www\core\Validation::validate($result->mail, 'MAIL')) : ?>
                                <a href="mailto:<?= $result->mail ?>"><?= $result->mail ?></a>
                            <?php else : ?>
                                <?= $result->mail ?>
                            <?php endif ?>
                        )</i></p></li>

                    </ul>
                </div>
            <?php endforeach ?>
        <?php else : ?>
            <div class="col2 card cardError">
                <p>
                    Aucun résultat trouvé pour <i>"<?= $params['command'] ?>"</i>...
                </p>
            </div>
        <?php endif ?>
</div>
