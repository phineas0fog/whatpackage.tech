<div class="header">

    <a href="<?= $root ?>"><img src="public/img/logo.png" class="logo"></a>
    <div class="inline">
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>

        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

</div>


<div class='main cols3'>
    <div class='error col-2'>
        <big>401</big>
        <h1>Seuls les utilisateurs connectés peuvent faire ceci...</h1>
        <h2><i>Vous pouvez vous <a href="<?php echo $root ?>/login">connecter</a>, ou <a href="<?php echo $root ?>/register">créer un compte</a>, ou <a href='<?php echo $root ?>'>revenir à l'accueil</a></i></h2>
    </div>
</div>
