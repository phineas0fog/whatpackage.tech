<?php $root = www\core\Config::getInstance()->get('ROOT_URL') ?>

<div class="header">

    <a href="<?= $root ?>"><img src="public/img/logo.png" class="logo"></a>
    <div class="inline">
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>

        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

</div>


<div class='main cols3'>
    <div class='error col-2'>
        <big>404</big>
        <h1>Ce que vous cherchez n'est pas là...</h1>
        <h2><i>Vous pouvez <a href='<?php echo $root ?>'>revenir à l'accueil</a>, ou chercher sur <a target='_blank' href='http://duckduckgo.com'>duckduckgo</a> ;)</i></h2>
    </div>
</div>
