<div class="header">

    <a href="<?= $root ?>"><img src="public/img/logo.png" class="logo"></a>
    <div class="inline">
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>

        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

</div>


<div class='main cols3'>
    <div class='error col-2'>
        <big>500</big>
        <h1>Le serveur a rencontré un erreur...</h1>
        <h2><i><a href='<?php echo $root ?>'>revenir à l'accueil</a><br/>
        Vous pouvez nous aider en prévenant le <a href="mailto:e.vanespen@protonmail.com">webmaster</a></i></h2>
    </div>
</div>
