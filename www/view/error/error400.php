<?php $root = www\core\Config::getInstance()->get('ROOT_URL') ?>

<div class="header">

    <a href="<?= $root ?>"><img src="public/img/logo.png" class="logo"></a>
    <div class="inline">
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>

        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

</div>


<div class='main cols3'>
    <div class='error col-2'>
        <big>400</big>
        <h1>La syntaxe de la requête est erronée...</h1>
        <h2><i><a href='<?php echo $root ?>'>revenir à l'accueil</a></i></h2>
    </div>
</div>
