<div class="sidebar">

    <?php include 'nav.php' ?>

    <?php if(isset($params['results']) AND sizeof($params['results']) > 0) : ?>
        <div class="col1 card cardHead cardStat">
        <ul>
            <li><div class="stat statResult"><div class="statLabl">Résultats pour <i>"<?= $params['command'] ?>"</i> : </div><div class="statVal"><?= sizeof($params['results']) ?></div></div></li>
        </ul>
        </div>
    <?php endif ?>

    <?php include 'stats.php' ?>
</div>
