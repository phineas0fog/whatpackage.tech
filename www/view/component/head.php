<meta charset="utf-8">
<title>whatpackage</title>

<?php if (www\core\Config::getInstance()->get('deploy') == 'dev'): ?>
    <link href="public/css/AAA-reset.css" rel="stylesheet">
    <link href="public/css/grid.css" rel="stylesheet">
    <link href="public/css/common.css" rel="stylesheet">
    <link href="public/css/header.css" rel="stylesheet">
    <link href="public/css/form.css" rel="stylesheet">
    <link href="public/css/button.css" rel="stylesheet">
    <link href="public/css/card.css" rel="stylesheet">
    <link href="public/css/about.css" rel="stylesheet">
    <link href="public/css/stats.css" rel="stylesheet">
    <link href="public/css/nav.css" rel="stylesheet">
    <link href="public/css/error.css" rel="stylesheet">
<?php elseif (www\core\Config::getInstance()->get('deploy') == 'prod'): ?>
    <link href="public/min/min.css" rel="stylesheet">
<?php endif ?>

<link href="public/fa/css/font-awesome.min.css" rel="stylesheet">
<link href="public/css/fontConfig.css" rel="stylesheet">

<link rel="icon" type="image/png" href="public/img/logo.png" />
