<div class="header">

    <a href="<?= $root ?>"><img src="public/img/logo.png" class="logo"></a>
    <div class="inline">
        <form class="form" action="<?= $root ?>/searchCommand" method="post">
            <input type="search" name="keyword" placeholder="Chercher une commande..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>

        <form class="form" action="<?= $root ?>/searchPackage" method="post">
            <input type="search" name="keyword" placeholder="Chercher un paquet..." required>
            <input type="submit" name="" value="" class="btn btnSearch">
        </form>
    </div>

</div>
