<div class="col1 card cardHead cardStat">
    <h1>Statistiques</h1>
    <?php if(isset($count)) : ?>
        <ul>
            <li><div class="stat"><div class="statLabl">Commandes indéxées : </div><div class="statVal"><?= $count ?></div></div></li>
            <li><div class="stat"><div class="statLabl">Requêtes :           </div><div class="statVal"><?= $visits ?></div></div></li>
        </ul>
    <?php endif ?>
</div>
