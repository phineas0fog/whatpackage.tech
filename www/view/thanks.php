<?php include 'component/header.php' ?>

<style media="screen">
    .error img { filter: drop-shadow(5px 5px 5px black); animation: zoom 1s}
    .error img:hover { cursor: pointer; }

    @keyframes zoom {
        0%{ transform: scale(0) rotate(0deg); }
        100%{ transform: scale(1) rotate(360deg);  }
    }
</style>

<div class='main cols3'>
    <div class='error col-2'>
        <h1 style="margin-top: -1vh">Merci pour votre contribution !</h1>
        <img src="public/ressources/badge.png" alt="" onclick="window.location.href='<?= $root ?>/public/ressources/badge.sh'">
    </div>
</div>
