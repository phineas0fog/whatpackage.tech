<?php

/**
 * @class HomeController
 *
 * ----------------------------------------
 * License AGPL custom : commercialization is prohibited
 * https://www.gnu.org/licenses/agpl-3.0.fr.html
 * @author : Evrard Van Espen
 * Creation : December 2017
 */

namespace www\controller;

use www\model\utils\PaquetUtils;
use www\core\Config;
use www\core\Validation;

class HomeController
{
    private $model;

    public function __construct()
    {
        session_start();
        $this->modelUtil = new PaquetUtils();
    }


    /**
     * display the view passed in arg, homeView by default
     * @param viewToCall the name of the view to display (home by default)
     * @return void
     */
    public function call($view='homeView', $params=[]) : void {
        include 'core/Counter.php';

        $count = $this->modelUtil->countAll();

        $page = Config::getInstance()->get('rep') . (Config::getInstance()->get('view'))[$view];

        include Config::getInstance()->get('basePage');
    }

    public function searchByCommand() {
        if($_SERVER["CONTENT_TYPE"] == "application/json") {
            $postBody = file_get_contents('php://input');
            $postBody = json_decode($postBody, true);
            if(!isset($postBody['keyword'])) {
                throw new \Exception("Bad request", 3);
            } else {
                $command = $postBody['keyword'];
            }
        } else {
            $command = Validation::validate($_POST['keyword'], 'TEXT');
        }

        $results = $this->modelUtil->searchByCommand($command);

        if($_SERVER["CONTENT_TYPE"] == "application/json") {
            header('Content-Type: application/json');
            echo json_encode($results);
            return;
        }

        $params['results'] = $results;
        $params['command'] = $command;
        $this->call('results', $params);
    }

    public function searchByPackage() {
        if($_SERVER["CONTENT_TYPE"] == "application/json") {
            $postBody = file_get_contents('php://input');
            $postBody = json_decode($postBody, true);
            if(!isset($postBody['keyword'])) {
                throw new \Exception("Bad request", 3);
            } else {
                $package = $postBody['keyword'];
            }
        } else {
            $package = Validation::validate($_POST['keyword'], 'TEXT');
        }

        $results = $this->modelUtil->searchByPackage($package);

        if($_SERVER["CONTENT_TYPE"] == "application/json") {
            header('Content-Type: application/json');
            echo json_encode($results);
            return;
        }

        $params['results'] = $results;
        $params['command'] = $package;
        $this->call('results', $params);
    }

    public function contrib() {
        $this->call('contrib');
    }

    public function about() {
        $this->call('about');
    }

    public function getCommandList() {
        $results = $this->modelUtil->getCommandList();

        if($_SERVER["CONTENT_TYPE"] == "application/json") {
            header('Content-Type: application/json');
            echo json_encode($results);
            return;
        }

        return $results;
    }

    public function thanks() {
        $this->call('thanks');
    }

    public function uploadResults() {
        $target_dir = "uploads/";
        $targetName = date('Y-m-d_H-i-s', time()). "-out.tar";
        $target_file = $target_dir . $targetName;
        $uploadOk = 1;
        $fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            if($fileType != 'tar') {
                $_SESSION['error'] = 'Le fichier n\'est pas au format .tar';
                $this->call('contrib');
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            $_SESSION['error'] = 'Le fichier existe déjà.';
            $this->call('contrib');
            $uploadOk = 0;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $_SESSION['error'] = 'Le fichier est trop gros.';
            $this->call('contrib');
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $_SESSION['error'] = 'Le fichier n\'a pas été envoyé.';
            $this->call('contrib');

        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $this->call('thanks');
            } else {
                $_SESSION['error'] = 'Le fichier n\'a pas été envoyé.';
                $this->call('contrib');
            }
        }
    }
}
