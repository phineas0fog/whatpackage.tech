<?php $root = www\core\Config::getInstance()->get('ROOT_URL'); ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include 'view/component/head.php' ?>
    </head>
    <body>

        <?php include $page ?>

    </body>

    <?php if (www\core\Config::getInstance()->get('deploy') == 'dev'): ?>
        <script>
            <?php include 'public/js/jquery.js' ?>
            <?php include 'public/js/tabbar.js' ?>
        </script>
    <?php elseif (www\core\Config::getInstance()->get('deploy') == 'prod'): ?>
        <script>
            <?php include 'public/min/min.js' ?>
        </script>
    <?php endif ?>

    <?php include 'view/component/error.php' ?>

</html>
